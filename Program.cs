﻿using System;
using System.Linq;
using System.Text;

class Program
{

    static void Main(string[] args) {
        Console.OutputEncoding = Encoding.Unicode;
        Console.InputEncoding = Encoding.Unicode;
        Console.ForegroundColor = ConsoleColor.Red;
        Console.Write("Введіть ім'я абітурієнта: ");
        Console.ResetColor();

        string name = Console.ReadLine();
        Console.ForegroundColor = ConsoleColor.Red;
        Console.Write("Введіть ідентифікаційний номер абітурієнта: ");
        Console.ResetColor();
        int idNum; 
        while (!int.TryParse(Console.ReadLine(), out idNum))
        {
            Console.WriteLine("Некоректне значення. Будь ласка, спробуйте ще раз.");
        }

        Console.ForegroundColor = ConsoleColor.Red;
        Console.Write("Введіть бали за підготовчі курси: ");
        Console.ResetColor();
        double coursePoints; 
        while (!double.TryParse(Console.ReadLine(), out coursePoints))
        {
            Console.WriteLine("Некоректне значення. Будь ласка, спробуйте ще раз.");
        }

        Console.ForegroundColor = ConsoleColor.Red;
        Console.Write("Введіть середній бал атестату: ");
        Console.ResetColor();
        double avgPoints;
        while (!double.TryParse(Console.ReadLine(), out avgPoints))
        {
            Console.WriteLine("Некоректне значення. Будь ласка, спробуйте ще раз.");
        }
        Console.ForegroundColor = ConsoleColor.Red;
        Console.Write("Введіть кількість результатів ЗНО: ");
        Console.ResetColor();
        int znoCount;
        while (!int.TryParse(Console.ReadLine(), out znoCount))
        {
            Console.WriteLine("Некоректне значення. Будь ласка, спробуйте ще раз.");
        }

        ZNO[] znoResults = new ZNO[znoCount];

        for (int i = 0; i < znoCount; i++)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write($"Введіть назву предмету ЗНО {i + 1}: ");
            Console.ResetColor();
            string subject = Console.ReadLine();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write($"Введіть бал з предмету ЗНО {subject}: ");
            Console.ResetColor();
            int points;
            while (!int.TryParse(Console.ReadLine(), out points))
            {
                Console.WriteLine("Некоректне значення. Будь ласка, спробуйте ще раз.");
            }
            znoResults[i] = new ZNO(subject, points);
        }

        Entrant entrant = new Entrant(name, idNum, coursePoints, avgPoints, znoResults);

        double compMark = entrant.GetCompMark();
        Console.ForegroundColor = ConsoleColor.Green;
        Console.WriteLine($"Абітурієнт {name} має конкурсний бал {compMark}");
    }



    public class Entrant
    {
        protected string Name;
        protected int IdNum;
        protected double CoursePoints;
        protected double AvgPoints;
        protected ZNO[] ZNOResults;

        public Entrant() { }

        public Entrant(string name, int idNum, double coursePoints, double avgPoints, ZNO[] znoResults)
        {
            Name = name;
            IdNum = idNum;
            CoursePoints = coursePoints;
            AvgPoints = avgPoints;
            ZNOResults = znoResults;
        }

        public Entrant(Entrant entrant)
        {
            Name = entrant.Name;
            IdNum = entrant.IdNum;
            CoursePoints = entrant.CoursePoints;
            AvgPoints = entrant.AvgPoints;
            ZNOResults = entrant.ZNOResults;
        }

        public string GetBestSubject()
        {
            string bestSubject = "";
            int bestPoints = 0;
            foreach (ZNO zno in ZNOResults)
            {
                int points = zno.GetPoints();
                if (points > bestPoints)
                {
                    bestSubject = zno.Subject;
                    bestPoints = points;
                }
            }
            return bestSubject;
        }

        public string GetWorstSubject()
        {
            string worstSubject = "";
            int worstPoints = int.MaxValue;
            foreach (ZNO zno in ZNOResults)
            {
                int points = zno.GetPoints();
                if (points < worstPoints)
                {
                    worstSubject = zno.Subject;
                    worstPoints = points;
                }
            }
            return worstSubject;
        }

        public double GetCompMark()
        {
            if (ZNOResults.Length < 3) return 0;

            double znoPoints = ZNOResults.OrderByDescending(zno => zno.Points)
                                          .Take(3)
                                          .Sum(zno => zno.Points * 0.25);

            double compMark = CoursePoints * 0.05 + AvgPoints * 0.1 + znoPoints * 0.4;

            return compMark;
        }

        public string GetName()
        {
            return Name;
        }

        public int GetIdNum()
        {
            return IdNum;
        }

        public double GetCoursePoints()
        {
            return CoursePoints;
        }

        public double GetAvgPoints()
        {
            return AvgPoints;
        }

        public ZNO[] GetZNOResults()
        {
            return ZNOResults;
        }

        public void SetName(string name)
        {
            Name = name;
        }

        public void SetIdNum(int idNum)
        {
            IdNum = idNum;
        }

        public void SetCoursePoints(double coursePoints)
        {
            CoursePoints = coursePoints;
        }

        public void SetAvgPoints(double avgPoints)
        {
            AvgPoints = avgPoints;
        }

        public void SetZNOResults(ZNO[] znoResults)
        {
            ZNOResults = znoResults;
        }
    }

    public class ZNO
    {
        public string Subject;
        public int Points;

        public ZNO(string subject, int points)
        {
            Subject = subject;
            Points = points;
        }

        public int GetPoints()
        {
            return Points;
        }
    }
}


